package com.simplilearn.lockme.application;

import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.Scanner;

import com.simplilearn.lockme.model.UserCredentials;
import com.simplilearn.lockme.model.Users;

public class Authentication 

{

                // This is input data
                private static Scanner keyboard;
                private static Scanner input;
                private static Scanner lockerInput;

                // This is out data
                private static PrintWriter output;
                private static PrintWriter lockerOutput;

                // This is Model to store data.
                private static Users users;
                private static UserCredentials usercredentials;

                public static void main(String[] args) {

                	initApp();            
                	welcomeScreen();
                    signInOptions();
                                

                }

                public static void signInOptions() {

                                System.out.println(" 1.Registration ");
                                System.out.println(" 2.Login / Sign-in");

                                int option = keyboard.nextInt();
                                
                                switch (option) {

                                case 1:
                                       registerUSer();
                                       break;
                                case 2:
                                       loginUser();
                                       break;
                                       
                                default:
                                System.out.println("Please select 1 or 2");
                                       break;

                                }
                                
                                keyboard.close();
                                input.close();

                }

                public static void lockerOptions(String inpUsername) {
                                
                                
                                System.out.println("Please select the action to be performed");
                                System.out.println("1. Fetch All Stored  Credentials");
                                System.out.println("2. Store my Credentials");
                                int option = keyboard.nextInt();
                                
                                switch (option) 
                                {
                                case 1:
                                                fetchCredentials(inpUsername);
                                                break;
                                case 2:
                                                storeCredentials(inpUsername);
                                                break;
                                default:
                                                System.out.println("Please select the available options");
                                                break;
                                }

                }

                public static void registerUSer() {
                	
                                System.out.println("*==========================================*");
                                System.out.println("****************************************8***");
                                System.out.println("*    Welcome to the Registration Page      *");
                                System.out.println("*****************************************8**");
                                System.out.println("*<><><><><><><><><><><><><><><><><><><><><>*");
                                System.out.println("*==========================================*");

                                System.out.println("Enter Username :");
                                String username = keyboard.next();
                                users.setUsername(username);
                                System.out.println("Enter Password :");
                                String password = keyboard.next();
                                users.setPassword(password);

                                output.println(users.getUsername());
                                output.println(users.getPassword());

                                System.out.println("User Registration Sucessfull!");

                                output.close();

                }

                public static void loginUser() {
                	System.out.println("*==================================*");
                    System.out.println("<><><><><><><><><><><><><><><><><><>");
                    System.out.println("*    Welcome to the Login Page     *");
                    System.out.println("************************************");
                    System.out.println("*<><><><><><><><><><><><><><><><><>*");
                    System.out.println("*==================================*");
                                System.out.println("Enter Username :");
                                String inpusername = keyboard.next();
                                boolean found = false;
                                while (input.hasNext() && !found) {
                                                if (input.next().equals(inpusername)) {

                                                                System.out.println("Enter Password : ");
                                                                String inpPassword = keyboard.next();

                                                                if (input.next().equals(inpPassword)) {

                                                                                System.out.println(" Login Sucessful ! : 200 ok");
                                                                                found = true;
                                                                                lockerOptions(inpusername);
                                                                                break;

                                                                }

                                                }

                                }
                                if (!found) {
                                                System.out.println("User Not Found: Login Failure : 404");
                                }

                }

                public static void welcomeScreen() {
                                System.out.println("*-----------------------------------------*");
                                System.out.println("*<><><><><><><><><><><><><><><><><><><><> *");
                                System.out.println("*        Welcome to Lockme.com            *");
                                System.out.println("*      Your Personal Digi Locker          *");
                                System.out.println("*<><><><><><><><><><><><><><><><><><><><> *");
                                System.out.println("*-----------------------------------------*");

                }

                // To store credentials
                public static void storeCredentials(String loggedInUser) 
                {

                                System.out.println("*-----------------------------------------*");
                                System.out.println(" <><><><><><><><><><><><><><><><><><><><><>");
                                System.out.println("*      Welcome to Digital Locker          *");
                                System.out.println("*       Store your CREDs here             *");
                                System.out.println("*<><><><><><><><><><><><><><><><><><><><><>");
                                System.out.println("*-----------------------------------------*");

                                usercredentials.setLoggedInUser(loggedInUser);

                                System.out.println("Enter Site Name :");
                                String SiteName = keyboard.next();
                                usercredentials.setSiteName(SiteName);

                                System.out.println("Enter Username :");
                                String Username = keyboard.next();
                                usercredentials.setUsername(Username);

                                System.out.println("Enter Password :");
                                String password = keyboard.next();
                                usercredentials.setPassword(password);

                                lockerOutput.println(usercredentials.getLoggedInUser());
                                lockerOutput.println(usercredentials.getSiteName());
                                lockerOutput.println(usercredentials.getUsername());
                                lockerOutput.println(usercredentials.getPassword());
                                
                                System.out.println("user Registration Sucessfull");
                                lockerOutput.close();
                                
                }

                // To fetch credentials
                public static void fetchCredentials(String inpusername)
                {

                                System.out.println("*------------------------------------*");
                                System.out.println("**************************************");
                                System.out.println("*      Fetching Your Credentials     *");
                                System.out.println("                                      ");
                                System.out.println("************************************ *");            
                                System.out.println("*------------------------------------*");
                                System.out.println(inpusername);
                                while(lockerInput.hasNext()) 
                                                if (lockerInput.next().equals(inpusername))
                                                {
                                                                System.out.println("Site Name: " +lockerInput.next());
                                                                System.out.println("Site User Name: " +lockerInput.next());
                                                                System.out.println("Site Passwrod: " +lockerInput.next());
                                                }
                }
                                
                

                public static void initApp() {
                                File dbfile = new File("Database.txt");
                                File lockerfile = new File("locker-file.txt");

                                try {

                                                // read data from file
                                                input = new Scanner(dbfile);
                                                // read from locker file

                                                lockerInput = new Scanner(lockerfile);
                                                // read data from keyboard

                                                keyboard = new Scanner(System.in);
                                                // out put
                                                output = new PrintWriter(new FileWriter(dbfile, true));
                                                lockerOutput = new PrintWriter(new FileWriter(lockerfile, true));

                                                users = new Users();
                                                usercredentials = new UserCredentials();

                                } catch (IOException e) {

                                                System.out.println("404 : file not found");

                                }

                }

}
